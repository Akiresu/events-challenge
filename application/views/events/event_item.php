        <div class="event_item" data-id="<?php echo $id; ?>">
            <?php form_open('form'); ?>
                <div class="ei_Dot"></div>
                <input type="text" class="ei_iDate" placeholder="Date" name="date" 
                    value="<?php echo $date; ?>" />
                <input type="text" class="ei_iTitle" placeholder="Title" name="title" 
                    value="<?php echo $title; ?>" />
                <div class="ei_Date"><?php echo $date; ?></div>
                <div class="ei_Title"><?php echo $title; ?></div>
                <div class="ei_Edit">Edit</div>
                <div class="ei_Delete">Delete</div>
                <button type="button" class="m_save">Save</button>
                <span class="m_cancel">Cancel</span>
            </form>
        </div>
