<!DOCTYPE html>
<html>
<head>
    <title>Event list</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>../css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>../css/fullcalendar.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
 	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://momentjs.com/downloads/moment.js"></script>
    <script src="<?php echo base_url(); ?>../js/notify.js"></script>
    <script src="<?php echo base_url(); ?>../js/script.js"></script>
    <script src="<?php echo base_url(); ?>../js/fullcalendar.js"></script>
</head>
<body>
    <div class="container">
	  <div class="calendar light">
	    <div class="calendar_header">
	      <div class="cl_add">
	          <i class="fas fa-plus"></i>
	        </div>
	      <h1 class = "header_title">Events</h1>
	    </div>
