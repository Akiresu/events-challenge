<?php
class Events_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_events($id = -1)
    {
        if ($id === -1)
        {
            $query = $this->db->get('events');
            return $query->result_array();
        }

        $query = $this->db->get_where('events', array('id' => $id));
        return $query->result();
    }

    public function get_events_by_date($from, $to = NULL)
    {
        $param = array('date >=' => $from);

        if (!is_null($to))
        {
            $param['date <='] = $to;
        }

        $query = $this->db->get_where('events', $param);
        return $query->result();
    }

    public function insert()
    {
        $data = array(
            'title' => $this->input->post('title'),
            'date' => $this->input->post('date')
        );

        $this->db->insert('events', $data); // values are escaped automatically
        return $this->db->insert_id();
    }

    public function update()
    {
        $id = $this->input->post('id');

        $data = array(
            'title' => $this->input->post('title'),
            'date' => $this->input->post('date')
        );

        $this->db->where('id', $id);
        return $this->db->update('events', $data); // values are escaped automatically
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        return $this->db->delete('events');
    }
}
