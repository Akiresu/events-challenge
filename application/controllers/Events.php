<?php
class Events extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('events_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $this->load->helper('form');
        $events = $this->events_model->get_events();

        $htmlItems = array();
        foreach ($events as $event)
        {
            $item = $this->load->view("events/event_item", $event, TRUE);
            array_push($htmlItems, $item);
        }

        $data['html_items'] = $htmlItems;

        $this->load->view('templates/header', $data);
        $this->load->view('events/index', $data);
        $this->load->view('events/calendar');
        $this->load->view('templates/footer');
    }

    public function create()
    {
        if ($this->validate())
        {
            echo $this->events_model->insert();
        }
    }

    public function update()
    {
        if ($this->validate())
        {
            $this->events_model->update(); 
        }
    }
 
    private function validate()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('date', 'Date', 
            'required|trim|callback__check_date_valid');
        $this->form_validation->set_rules('title', 'Title', 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('events/errors');
            return FALSE;
        }

        return TRUE;
    }

    function delete()
    {
        $this->events_model->delete();
    }

    public function get_empty_event_html()
    {
        $this->load->helper('form');

        $data['id'] = "-1";
        $data['date'] = "";
        $data['title'] = "";

        $this->load->view('events/event_item', $data);
    }

    public function _check_date_valid($date)
    {
        $date_format = "Y-m-d";
        $this->form_validation->set_message('_check_date_valid', "Please enter a valid date");
        $d = DateTime::createFromFormat($date_format, $date);

        return ($d && $d->format($date_format) === $date || $date == '');
    }

    public function get_events_json()
    {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        $db_events = array();

        if (empty($start) && empty($end))
        {
            $db_events = $this->events_model->get_events();
        }
        else
        {
            $db_events = $this->events_model->get_events_by_date($start, $end);
        }

        $events = array();
        foreach ($db_events as $db_event)
        {
            array_push($events, array(
                'title' => $db_event->title,
                'start' => $db_event->date
            ));
        }

        echo (json_encode($events));
    }
}