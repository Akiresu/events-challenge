$(document).ready(function() {
	var is_editing = false;
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

    $( "input.ei_iDate" ).datepicker({ dateFormat: 'yy-mm-dd' });

    function enable_editing(item) {
        item.addClass("selected");
        item.children(".ei_Date").addClass("hidden");
        item.children(".ei_Title").addClass("hidden");
        is_editing = true;
    }

    function disable_editing(item) {
        item.removeClass("selected");
        item.children(".ei_Date").removeClass("hidden");
        item.children(".ei_Title").removeClass("hidden");
        is_editing = false;
    }

    function edit_click(item) {
        enable_editing(item);
    };

    function cancel_click(item) {
        disable_editing(item);
    }

    function save_click(item) {
        let id = item.data("id");
        let date = item.children(".ei_iDate").val();
        let title = item.children(".ei_iTitle").val();

        if (id === -1) {
            $.post(baseUrl + "/index.php/events/create", 
                {date: date, title: title}, function (data) {
                let isNum = /^\d+$/.test(data);
                if (isNum) {
                    item.children(".ei_Date").html(date);
                    item.children(".ei_Title").html(title);
                    item.data("id", data);
                    disable_editing(item);
                } else {
                    $.notify(data, {position: "bottom center"});
                }
            });
        } else {
            $.post(baseUrl + "/index.php/events/update", 
                {id: id, date: date, title: title}, function (data) {
                if (data === "") {
                    item.children(".ei_Date").html(date);
                    item.children(".ei_Title").html(title);
                    disable_editing(item);
                } else {
                    $.notify(data, {position: "bottom center"});
                }
            });
        }
    }

    function delete_click(item) {
        let id = item.data("id");
        $.post(baseUrl + "/index.php/events/delete", {id: id}, function (data) {
            item.slideUp("slow", function() { item.remove(); });
        });
    }

    $(".ei_Edit").click(function() {
        if (!is_editing)
        {
            edit_click($(this).parents(".event_item"));
        }
    });

    $(".m_save").click(function() {
        save_click($(this).parents(".event_item"));
    });

    $(".m_cancel").on("click", function(){
        cancel_click($(this).parents(".event_item"));
	});

    $(".ei_Delete").click(function(){
        delete_click($(this).parents(".event_item"));
    });

	$(".cl_add").click(function () {
        if (!is_editing)
        {
            is_editing = true;
            $.ajax({
                method: "POST",
                url: baseUrl + "/index.php/events/get_empty_event_html"
            })
            .done(function(data) {
                $("#event_list").prepend(data);
                let current_item = $("#event_list").children().first();

                $(".ei_iDate").datepicker({ dateFormat: 'yy-mm-dd' });
                edit_click(current_item)

                $(".event_item").on("click", ".m_cancel", function(){
                    current_item.remove();
                    is_editing = false;
                });

                $(".event_item").on("click", ".m_save", function(){
                    save_click(current_item);
                });
            })
            .fail(function() {
                $.notify("Loading of event element has failed", 
                     {position: "bottom center"});
                is_editing = false;
            });
        }
	});

    // FullCalendar

    $('#calendar').fullCalendar({
        header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        events: {
            url: 'events/get_events_json'
        },
        loading: function(bool) {
            $('#loading').toggle(bool);
        }
    });
});
